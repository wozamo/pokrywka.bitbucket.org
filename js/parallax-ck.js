/*
 Plugin: jQuery Parallax
 Version 1.1.3
 Author: Ian Lunn
 Twitter: @IanLunn
 Author URL: http://www.ianlunn.co.uk/
 Plugin URL: http://www.ianlunn.co.uk/plugins/jquery-parallax/

 Dual licensed under the MIT and GPL licenses:
 http://www.opensource.org/licenses/mit-license.php
 http://www.gnu.org/licenses/gpl.html
 */(function(e){var t=e(window),n=t.height();t.resize(function(){n=t.height()});e.fn.parallax=function(r,i,s){function l(){var s=t.scrollTop();a.each(function(){var t=e(this),f=t.offset().top,l=o(t);if(f+l<s||f>s+n)return;a.css("backgroundPosition",r+" "+Math.round((u-s)*i)+"px")})}var o,u,a=e(this),f=0;a.each(function(){u=a.offset().top});s?o=function(e){return e.outerHeight(!0)}:o=function(e){return e.height()};if(arguments.length<1||r===null)r="50%";if(arguments.length<2||i===null)i=.1;if(arguments.length<3||s===null)s=!0;t.bind("scroll",l).resize(l);l()}})(jQuery);